#!/bin/sh

# regenerate status page
yarn run gl-pages:generate

# make sure everything looks fine
cd dist && npx http-server -p 8080